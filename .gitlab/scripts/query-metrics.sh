#!/usr/bin/env sh

get_metrics() {
  local alias="${1}"
  local query="${2}"

  echo "Get metrics for ${alias}"

  curl -s "http://prometheus:9090/api/v1/query_range" \
       -F start="${start_datetime}" \
       -F end="${end_datetime}" \
       -F step="5s" \
       -F query="${query}" > "${alias}.metrics.json"
}

get_metrics cpu "sum by(exported_mode) (rate(node_cpu_seconds_total[1m]))"
get_metrics cpu_guest "sum by(exported_mode) (rate(node_cpu_guest_seconds_total[1m]))"
get_metrics memory "node_memory_MemAvailable_bytes / node_memory_MemTotal_bytes"
get_metrics filesystem_size "sum by(mountpoint) (node_filesystem_free_bytes / node_filesystem_size_bytes)"
get_metrics read_iops "sum by(device) (rate(node_disk_reads_completed_total[1m]))"
get_metrics write_iops "sum by(device) (rate(node_disk_writes_completed_total[1m]))"
get_metrics network_receive "sum by(device) (rate(node_network_receive_bytes_total[1m]))"
get_metrics network_transmit "sum by(device) (rate(node_network_transmit_bytes_total[1m]))"

